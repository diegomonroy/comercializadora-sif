// JavaScript Document

/* ************************************************************************************************************************

COMERCIALIZADORA SIF

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// CSS

gulp.task('css', function () {
	return gulp
		.src('wp-content/themes/COMERCIALIZADORA-SIF/assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('wp-content/themes/COMERCIALIZADORA-SIF/build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('wp-content/themes/COMERCIALIZADORA-SIF/assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('wp-content/themes/COMERCIALIZADORA-SIF/build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('wp-content/themes/COMERCIALIZADORA-SIF/assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('wp-content/themes/COMERCIALIZADORA-SIF/build'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['wp-content/themes/COMERCIALIZADORA-SIF/assets/css/**/*.scss'], ['css']);
	gulp.watch(['wp-content/themes/COMERCIALIZADORA-SIF/assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['css', 'js', 'image', 'watch']);