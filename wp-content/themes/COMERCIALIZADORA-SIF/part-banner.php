<!-- Begin Banner -->
	<section class="banner wow bounceInUp" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos', 'mision-vision' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'nuestro-equipo' ) ) ) : dynamic_sidebar( 'banner_nuestro_equipo' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'contactenos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->