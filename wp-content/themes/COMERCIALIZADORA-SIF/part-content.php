<?php get_template_part( 'part', 'banner' ); ?>
<?php if ( is_page( array( 'quienes-somos', 'mision-vision', 'nuestro-equipo' ) ) ) : ?>
<?php get_template_part( 'part', 'submenu' ); ?>
<?php endif; ?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->