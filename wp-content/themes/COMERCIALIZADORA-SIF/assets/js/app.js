// JavaScript Document

/* ************************************************************************************************************************

COMERCIALIZADORA SIF

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Cotizar */
	jQuery( 'label[for="rqa-message"]' ).html( 'Mensaje y teléfono de contacto <abbr class="required" title="required">*</abbr>');
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'insertar correo aquí' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
});