<!-- Begin Top -->
	<section class="top wow bounceInDown" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns show-for-small-only">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 columns relative">
				<div class="row align-right">
					<div class="small-3 medium-2 columns">
						<?php dynamic_sidebar( 'cart' ); ?>
					</div>
					<div class="small-9 medium-4 columns">
						<?php dynamic_sidebar( 'search' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- End Top -->