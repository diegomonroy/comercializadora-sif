<!-- Begin Top Data -->
	<section class="top_data wow bounceInDown" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_data' ); ?>
			</div>
		</div>
	</section>
<!-- End Top Data -->