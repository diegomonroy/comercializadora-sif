<!-- Begin Submenu -->
	<section class="submenu wow bounceInUp" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( is_page( array( 'quienes-somos', 'mision-vision', 'nuestro-equipo' ) ) ) : ?>
				<?php
				wp_nav_menu(
					array(
						'menu_class' => 'menu align-center',
						'container' => false,
						'theme_location' => 'quienes-somos-menu',
						'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
					)
				);
				?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<!-- End Submenu -->